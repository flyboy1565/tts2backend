#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   conftest.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   16 Dec 2022

@brief  config for tests and population of test database

Copyright © 2022 Dj Djundjila, TTS Rebuild Committee

tts2backend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tts2backend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import pytest

import tts2backend
